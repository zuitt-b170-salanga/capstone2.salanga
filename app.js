/* B170-salanga-ecommerce app.js */

// setting up dependencies
const express = require("express")
const mongoose = require("mongoose")
const cors = require("cors")

// access to routes
const userRoutes = require("./routes/userRoutes")
const treatmentRoutes = require("./routes/treatmentRoutes")
const orderRoutes = require("./routes/orderRoutes")
// !! ADDED ON 09JUN2022
const cartRoutes = require("./routes/cartRoutes")



// server
const app = express()
const port = 4000

app.use(cors())   //  allows all origins/domains to access the backend application

app.use(express.json())
app.use(express.urlencoded( { extended:true } ))




mongoose.connect("mongodb+srv://mhsalanga:Koykoy22042022@wdc028-course-booking.fogfn.mongodb.net/B170-salanga-ecommerce?retryWrites=true&w=majority",
		{
		useNewUrlParser:true,
		useUnifiedTopology:true	
		})

let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection Error"));
db.once("open", () => console.log("We're connected to the database"))




 app.use("/api/users", userRoutes);       
 app.use("/api/treatments", treatmentRoutes);  
 app.use("/api/orders", orderRoutes);
// !! ADDED ON 09JUN2022
app.use("/api/carts", cartRoutes);




//app.listen(port, () => console.log(`API now online at port ${port}`))


// https://lit-plateau-63513.herokuapp.com
// !!! UNCOMMENT before pushing updates on gitlab; HEROKU & VERCEL afterwards
app.listen(process.env.PORT || port, () => console.log(`API now online at port ${process.env.PORT ||port}`))







//app.listen(process.env.PORT || port, () => console.log(`API now online at port ${process.env.PORT ||port}`))
//******************************* 
// proces.env.PORT works if you are deploying the api in a host like Heroku. this  code allows the app to use 
	//the environment of that host (Heroku) in running the server