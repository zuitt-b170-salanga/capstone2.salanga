/* B170-salanga-ecommerce orderRoutes.js */

const express = require ("express")

////
const Treatment = require("../models/Treatment.js")
const User = require("../models/User.js")
const Order = require("../models/Order.js")
////

const router = express.Router()

const auth = require("../auth.js")
const orderController = require("../controllers/orderController.js")

// ********************************************
// Get Orders of Specific User  (non-Admin ONLY) 

router.get("/myorders", auth.verify,(req, res) => {
	const userData = auth.decode(req.headers.authorization)
	orderController.getOrder(req.body, userData).then(result => res.send(result))
})

// Get Order - GET - http://localhost:4000/api/orders/6273a7c67f74eec49608a008
//auth (user itself or admin)
//{
//     "userId": "6273a7c67f74eec49608a008"
// }
//output:
// [
//     {
//         "_id": "6278632268a9c59803d0b2db",
//         "userId": "6273a7c67f74eec49608a008",
//         "treatmentId": "627338638d7f3d3c72e74b98",
//         "treatmentName": "Manual therapy",
//         "quantity": 1,
//         "price": 76,
//         "totalBill": 76,
//         "__v": 0
//     },
//     {
//         "_id": "6278634d68a9c59803d0b2e4",
//         "userId": "6273a7c67f74eec49608a008",
//         "treatmentId": "627338638d7f3d3c72e74b98",
//         "treatmentName": "Manual therapy",
//         "quantity": 2,
//         "price": 76,
//         "totalBill": 152,
//         "__v": 0
//     }
// ]
// ********************************************

// Get All Orders ( Admin ONLY ====)

router.get("/getallorders", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	orderController.getAllOrders(userData).then(resultFromController => res.send(resultFromController))
})

// Get All Orders - GET -  http://localhost:4000/api/orders/getallorders
// (auth ==> non-Admin)
//output:
//false
// (auth ==> Admin)
//output:
//[
//     {
//         "_id": "6278632268a9c59803d0b2db",
//         "userId": "6273a7c67f74eec49608a008",
//         "treatmentId": "627338638d7f3d3c72e74b98",
//         "treatmentName": "Manual therapy",
//         "quantity": 1,
//         "price": 76,
//         "totalBill": 76,
//         "__v": 0
//     },
//     {
//         "_id": "6278634d68a9c59803d0b2e4",
//         "userId": "6273a7c67f74eec49608a008",
//         "treatmentId": "627338638d7f3d3c72e74b98",
//         "treatmentName": "Manual therapy",
//         "quantity": 2,
//         "price": 76,
//         "totalBill": 152,
//         "__v": 0
//     },
//     {
//         "_id": "62790f7d6af683a0a275d528",
//         "userId": "6273a7c67f74eec49608a008",
//         "treatmentId": "627335818d7f3d3c72e74b92",
//         "treatmentName": "Light Therapy/Phototherapy",
//         "quantity": 2,
//         "price": 200,
//         "totalBill": 400,
//         "__v": 0
//     },
//     {
//         "_id": "627911136af683a0a275d532",
//         "userId": "6273a72c7f74eec49608a006",
//         "treatmentId": "627335818d7f3d3c72e74b92",
//         "treatmentName": "Light Therapy/Phototherapy",
//         "quantity": 2,
//         "price": 200,
//         "totalBill": 400,
//         "__v": 0
//     }
// ]

// ********************************************
// Create Order (non-Admin ONLY)


router.post("/createorder", auth.verify, (req, res) => {
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		treatmentId: req.body.treatmentId,
		treatmentName: req.body.treatmentName,
		quantity: req.body.quantity,
		price: req.body.price
	}
	orderController.createOrder(data).then(result => res.send (result))
})



//
//User Checkout - POST - http://localhost:4000/api/orders/checkout ==
// treatment:isActive=true   ----> 
//{
// "userId":"6273a7c67f74eec49608a008",
// "treatmentId":"627338638d7f3d3c72e74b98",
// "treatmentName":"Manual therapy",
// "quantity":2,
// "price":76
//}
//Admin
//output:
//false
//non-Admin 
//
//output:
//true
// ********************************************





// ********************************************
/* !!!! ADDED ON 09JUN2022  */

// checkout the items in the cart of the logged in user
router.post('/', auth.verify, (req, res) => {
	const payload = auth.decode(req.headers.authorization);
	
	orderController.checkout(payload).then(resultFromController => {
		res.send(resultFromController)
	})
});



// ********************************************

module.exports = router
