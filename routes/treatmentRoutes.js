/* B170-salanga-ecommerce treatmentRoutes.js */

const express = require("express")

////
const Treatment = require("../models/Treatment.js")
////

const router = express.Router()

const auth = require("../auth.js")
const treatmentController = require("../controllers/treatmentController.js")

// ********************************************
//Retrieve all active treatments (Admin & non-Admin)  

router.get("/activetreatments", (req, res) => {
	treatmentController.getActiveTreatments().then(resultFromController => res.send(resultFromController))
})

// Get All Active Treatments - GET - 
// output:
//[
//     {
//         "_id": "627335818d7f3d3c72e74b92",
//         "treatmentName": "Light Therapy/Phototherapy",
//         "description": "Promotes tissue repair and natural healing in and around bones, joints, muscles, ligaments and tendons.",
//         "price": 200,
//         "isActive": true,
//         "createdOn": "2022-05-05T02:24:51.584Z",
//         "patients": [],
//         "__v": 0
//     },
//     {
//         "_id": "6273373b8d7f3d3c72e74b95",
//         "treatmentName": "Diathermy",
//         "description": "Uses a high-frequency electric current to stimulate heat generation within body tissues.",
//         "price": 50,
//         "isActive": true,
//         "createdOn": "2022-05-05T02:24:51.584Z",
//         "patients": [],
//         "__v": 0
//     },
//     {
//         "_id": "627338638d7f3d3c72e74b98",
//         "treatmentName": "Manual therapy",
//         "description": "The skilled application of passive movement to a joint either within or beyond its active range of movement.",
//         "price": 76,
//         "isActive": true,
//         "createdOn": "2022-05-05T02:24:51.584Z",
//         "patients": [],
//         "__v": 0
//     }
// ]
// ********************************************
// Retrieve single treatment (Admin & non-Admin)

router.get("/:treatmentId", (req, res) => {
	console.log(req.params.treatmentId);
	treatmentController.getTreatment(req.params.treatmentId).then(result => res.send(result))
})

//Get a Treatment - GET - http://localhost:4000/api/treatments/627335818d7f3d3c72e74b92
//auth
//output:
//{
//     "_id": "627335818d7f3d3c72e74b92",
//     "treatmentName": "Light Therapy/Phototherapy",
//     "description": "Promotes tissue repair and natural healing in and around bones, joints, muscles, ligaments and tendons.",
//     "price": 200,
//     "isActive": true,
//     "createdOn": "2022-05-05T02:24:51.584Z",
//     "patients": [],
//     "__v": 0
// }
// ********************************************
// Create treatment (Admin ONLY)

router.post("/", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	treatmentController.addTreatment(req.body, userData).then(resultFromController => res.send(resultFromController))
})


//Create Treatment - POST -	http://localhost:4000/api/treatments
//
//auth
//{
//     "treatmentName": "Light Therapy/Phototherapy",
//     "description":"Promotes tissue repair and natural healing in and around bones, joints, muscles, ligaments and tendons.",
//     "price": 200
// }
//output:
//Successfully Created Treatment
//
// ********************************************
// Update treatment (Admin ONLY)   

router.put("/:treatmentId", auth.verify, (req, res) => {
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		treatmentId: req.params.treatmentId
	}
	treatmentController.updateTreatment(data, req.body).then(result => res.send(result))
})



//Update Treatment - PUT - http://localhost:4000/api/treatments/62733a9c8d7f3d3c72e74b9b
//auth info comi
//{
//     "isActive": false
// }
//output:
//true
// ********************************************
// Get All Treatments

router.get("/", (req, res) => {
	treatmentController.getAllTreatments().then(resultFromController => res.send(resultFromController))
})

// Get All Treatments - GET - http://localhost:4000/api/treatments
//[
//     {
//         "_id": "627335818d7f3d3c72e74b92",
//         "treatmentName": "Light Therapy/Phototherapy",
//         "description": "Promotes tissue repair and natural healing in and around bones, joints, muscles, ligaments and tendons.",
//         "price": 200,
//         "isActive": true,
//         "createdOn": "2022-05-05T02:24:51.584Z",
//         "patients": [],
//         "__v": 0
//     },
//     {
//         "_id": "6273373b8d7f3d3c72e74b95",
//         "treatmentName": "Diathermy",
//         "description": "Uses a high-frequency electric current to stimulate heat generation within body tissues.",
//         "price": 50,
//         "isActive": true,
//         "createdOn": "2022-05-05T02:24:51.584Z",
//         "patients": [],
//         "__v": 0
//     },
//     {
//         "_id": "627338638d7f3d3c72e74b98",
//         "treatmentName": "Manual therapy",
//         "description": "The skilled application of passive movement to a joint either within or beyond its active range of movement.",
//         "price": 76,
//         "isActive": true,
//         "createdOn": "2022-05-05T02:24:51.584Z",
//         "patients": [],
//         "__v": 0
//     },
//     {
//         "_id": "62733a9c8d7f3d3c72e74b9b",
//         "treatmentName": "Group therapy",
//         "description": "The treatment of 2-6 patients, who are performing the same or similar activities and are supervised by a therapist or an assistant.",
//         "price": 40,
//         "isActive": false,
//         "createdOn": "2022-05-05T02:24:51.584Z",
//         "patients": [],
//         "__v": 0
//     }
// ]
// ********************************************
// Archive Treatment (Admin ONLY)   -----

router.put("/:treatmentId/archive", auth.verify, (req, res) => {
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		treatmentId: req.params.treatmentId
	}
	treatmentController.archiveTreatment(data, req.body).then(result => res.send(result))
})


//Archive Treatment - PUT - http://localhost:4000/api/treatments/62733a9c8d7f3d3c72e74b9b/archive
//auth
//{
//     "isActive": false
// }
//output:
//{
//     "_id": "62733a9c8d7f3d3c72e74b9b",
//     "treatmentName": "Group therapy",
//     "description": "The treatment of 2-6 patients, who are performing the same or similar activities and are supervised by a therapist or an assistant.",
//     "price": 40,
//     "isActive": false,
//     "createdOn": "2022-05-05T02:24:51.584Z",
//     "patients": [],
//     "__v": 0
// }
// ********************************************



// ********************************************

module.exports = router