/* B170-salanga-ecommerce userRoutes.js */

const express = require ("express")

////
const User = require("../models/User.js")
////

const router = express.Router()

const auth = require("../auth.js")
const userController = require("../controllers/userController.js")

// ********************************************
// User registration (Admin & non-Admin)

router.post("/reguser", (req,res) => {
	userController.registerUser(req.body).then(resultFromController => res.send (resultFromController))
})

//Register User - POST - http://localhost:4000/api/users/reguser
//{
//     "firstName":"Lindsay",
//     "lastName": "Smith",
//     "age":"33",
//     "gender":"Female",
//     "email":"lsmith@mail.com",
//     "password":"lindsay123",
//     "referringPhysician":"I am a Physical Therapist",
//     "diagnosis":"N/A",
//     "isAdmin": false
// }
//output:
//true
// set isAdmin to true in mongodb
// ********************************************
// User authentication/login (Admin & non-Admin)

router.post("/veruser", (req, res) => {
	userController.userAuthenticate(req.body).then(resultFromController => res.send (resultFromController))
})

//User Login - POST -  http://localhost:4000/api/users/veruser
// {
//      "email":"lsmith@mail.com",
//      "password":"lindsay123"
// }
//output:
//{
//     "access": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYyNzJjYzFhZWZjNWZkNWQ0ZDY4ODk2OSIsImVtYWlsIjoibHNtaXRoQG1haWwuY29tIiwiaXNBZG1pbiI6dHJ1ZSwiaWF0IjoxNjUxNzAzNzU3fQ.ASxhmY2Jk3h94uT2peLVxWU_foIEhA07peffdfVpP2g"
//}

// ********************************************

// checking of email  ----->
router.post("/checkEmail", (req, res)=> {
	userController.checkEmail(req.body).then(resultFromController => res.send(resultFromController))
})

// ********************************************
// Set User as admin (Admin ONLY)

// router.post("/checkout", auth.verify, (req, res) => {
// 	let data = {
// 		userId: auth.decode(req.headers.authorization).id,
// 		isAdmin: auth.decode(req.headers.authorization).isAdmin,
// 		treatmentId: req.body.treatmentId,
// 		treatmentName: req.body.treatmentName,
// 		quantity: req.body.quantity,
// 		price: req.body.price
// 	}
// 	orderController.createOrder(data).then(result => res.send (result))
// })

router.put("/:userId/setAsAdmin", auth.verify, (req, res) => {
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	let userData = {
		userId: req.params.userId
		//isAdmin: req.body.isAdmin
	}
	userController.changeToAdmin(data, userData, req.body).then(result => res.send(result))
})

// router.put("/:userId/setAsAdmin", auth.verify, (req, res) => {
// 	const userData = auth.decode(req.headers.authorization)
// 	userController.changeToAdmin(userData, req.body).then(result => res.send(result))
// })

// Change to Admin - PUT - http://localhost:4000/api/users/6273a6ad7f74eec49608a004/setAsAdmin
//auth
//{
//     "isAdmin": true
// }
//output:
// {
//     "_id": "6273a6ad7f74eec49608a004",
//     "firstName": "Connor",
//     "lastName": "Randall",
//     "age": "30",
//     "gender": "Male",
//     "email": "crandall@mail.com",
//     "password": "$2b$10$xKris1mpA15qcFaXCrTNL.s0ZMc.0./4kOSQoyNEnV0lzczIe16RW",
//     "referringPhysician": "I am a Physical Therapist",
//     "diagnosis": "N/A",
//     "isAdmin": true,
//     "treatments": [],
//     "__v": 0
// }
// ********************************************
// Get User Profile

// auth.verify -----> ensures that a user is logged in before proceeding to the nxt part of the code; 
	//this is the verify function inside the auth.js 
router.get("/details", auth.verify, (req,res) => {
	// decode - decrypts the token inside the authorization (w/c is in the headers of the rqst)
	// req.headers.authorization - contains the token that was created for the user
	const userData = auth.decode(req.headers.authorization)
	console.log(userData)
	userController.getProfile({userId:userData.id}).then(resultFromController => res.send(
		resultFromController))

})
// ********************************************
//// Get all users

router.get("/", (req, res) => {
	userController.getAllUsers().then(resultFromController => res.send(resultFromController))
})

// Get All Users - GET - http://localhost:4000/api/users
//output:
// [
//     {
//         "_id": "6272cc1aefc5fd5d4d688969",
//         "firstName": "Lindsay",
//         "lastName": "Smith",
//         "age": "33",
//         "gender": "Female",
//         "email": "lsmith@mail.com",
//         "password": "$2b$10$G.JpLEKI/PPdvrDc2.8vPu0HkIB9XwefIEk/Paa5EM19r9LjpPxNi",
//         "referringPhysician": "I am a Physical Therapist",
//         "diagnosis": "N/A",
//         "isAdmin": true,
//         "treatments": [],
//         "__v": 0
//     },
//     {
//         "_id": "6273a6ad7f74eec49608a004",
//         "firstName": "Connor",
//         "lastName": "Randall",
//         "age": "30",
//         "gender": "Male",
//         "email": "crandall@mail.com",
//         "password": "$2b$10$xKris1mpA15qcFaXCrTNL.s0ZMc.0./4kOSQoyNEnV0lzczIe16RW",
//         "referringPhysician": "I am a Physical Therapist",
//         "diagnosis": "N/A",
//         "isAdmin": false,
//         "treatments": [],
//         "__v": 0
//     },
//     {
//         "_id": "6273a72c7f74eec49608a006",
//         "firstName": "Alicia",
//         "lastName": "Oliveira",
//         "age": "25",
//         "gender": "Female",
//         "email": "aoliveira@mail.com",
//         "password": "$2b$10$/Hv6C9gssmWTbqT.BqT/v.nHj2TP99oBS.4/HTQXCgtbfIB46qRxW",
//         "referringPhysician": "Dr. Bryan Tynes",
//         "diagnosis": "Post Covid",
//         "isAdmin": false,
//         "treatments": [],
//         "__v": 0
//     },
//     {
//         "_id": "6273a7c67f74eec49608a008",
//         "firstName": "Seth",
//         "lastName": "Alvarez",
//         "age": "71",
//         "gender": "Male",
//         "email": "salvarez@mail.com",
//         "password": "$2b$10$nh37wQeGPTnJKh0qFvQGqeG5lyjPx/GBAU8VxK52yUlmZUsFVL6ra",
//         "referringPhysician": "Dr. Terri Francisco",
//         "diagnosis": "Total Hip Replacement",
//         "isAdmin": false,
//         "treatments": [],
//         "__v": 0
//     }
// ]



// ********************************************

module.exports = router
