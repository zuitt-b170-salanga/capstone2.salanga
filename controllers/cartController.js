/* B170-salanga-ecommerce cartController.js*/

/* !!!! ADDED ON 09JUN2022  */

const Cart = require("../models/Cart")
const User = require("../models/User.js")
const Treatment = require("../models/Treatment.js")


module.exports.getCart = (payload) => {

	return Cart.find({userId: payload.id}).then(result => {
		return result;
	})
}

module.exports.addToCart = async (data) => {
	const {treatmentId, quantity, url} = data.reqBody;

	// findOne returns null (falsy) if nothing is found
	let cart = await Cart.findOne({userId: data.payload.id});
	let treatment = await Treatment.findOne({_id: treatmentId});

	if(!treatment) {
		return 'Treatment not Found!';
	}
	
	const {price, name} = treatment;

	// If cart already exists for the user
	if(cart) {
		let treatmentIndex = cart.treatments.findIndex(p => p.treatmentId == treatmentId);

		// Check to see if item is already in the cart. If -1, it doesn't exist.
		if(treatmentIndex > -1) {
			let treatmentItem = cart.treatments[treatmentIndex]; 
			treatmentItem.quantity += quantity;
			cart.treatments[treatmentIndex] = treatmentItem;
		}
		else {
			cart.treatments.push({treatmentId: treatmentId, name: name, quantity: quantity, price: price})
		}
		cart.bill += price * quantity;

		return cart.save().then((result, err) => {
			if(err) {
				return false;
			}
			else {
				return true;
			}
		})

	}
	else {
		// There is no cart existing so create one. 
		cart = new Cart({
			userId: data.payload.id,
			treatments: [{treatmentId: treatmentId, name: name, quantity: quantity, price: price}],
			bill: price * quantity
		});

		return cart.save().then((result, err) => {
			if(err) {
				return false;
			}
			else {
				return true;
			}
		})
	}
}

module.exports.deleteCart = (userId) => {
	return Cart.deleteOne({userId: userId}).then(result => {
		return result;
	})
}


module.exports.removeTreatment = async (data) => {
	const {userId, treatmentId} = data;
	let cart = await Cart.findOne({userId: userId});
	let treatmentIndex = cart.treatments.findIndex(p => p.treatmentId == treatmentId);

	// Product is in the cart
	if(treatmentIndex > -1) {
		let treatmentItem = cart.treatments[treatmentIndex];
		cart.bill -= treatmentItem.quantity*treatmentItem.price;
        cart.treatments.splice(treatmentIndex,1);
	}
	return cart.save().then((result, err) => {
		if(err) {
			return false;
		}
		else {
			return result;
		}
	})

}

