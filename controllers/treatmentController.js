/* B170-salanga-ecommerce treatmentController.js */

const User = require("../models/User.js")
const Treatment = require("../models/Treatment.js")

const auth = require("../auth.js")
const bcrypt = require("bcrypt")

// ********************************************
//Retrieve all active treatments ( Admin & non-Admin)  

module.exports.getActiveTreatments = () => {
	return Treatment.find({isActive:true}).then((result, error) => {
		if (error) {
			console.log(error)
		} else {
			return result
		}
	})
}

// ********************************************
// Retrieve single treatment ( Admin & non-Admin)

module.exports.getTreatment = (reqParams) => {
	return Treatment.findById(reqParams).then(result => {
		return result
	})
}

// ********************************************
// Create treatment (Admin ONLY)

/*router.post("/", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	treatmentController.addTreatment(req.body, userData).then(resultFromController => res.send(resultFromController))
})*/

module.exports.addTreatment = (reqBody, userData) => {

	return User.findById(userData.userId).then(result => {

		if (userData.isAdmin === false) {
			//return "PERMISSION DENIED"
			return false
		} else {
			let newTreatment = new Treatment ({
				treatmentName: reqBody.treatmentName,
				description: reqBody.description,
				price: reqBody.price
			})
			return newTreatment.save().then((treatment, error) => {
				if (error){
					return false
				} else {
					return "Successfully Created Treatment"
				}
			})
		}
	})
}

//Create Treatment - POST -	http://localhost:4000/api/treatments
//
//auth(non-Admin)
// {
//     "treatmentName": "Mechanical Traction",
//     "description":"The force used to create a degree of tension of soft tissues and/or to allow for separation between joint surfaces.",
//     "price": 20
// }
//output:
//false
// ********************************************
// Update treatment (Admin ONLY)

/*router.put("/:treatmentId", auth.verify, (req, res) => {
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		treatmentId: req.params.treatmentId
	}
	treatmentController.updateTreatment(data, req.body).then(result => res.send(result))
})*/

module.exports.updateTreatment = (data, reqBody) => {
	
	return User.findById(data.userId).then(result => {
		if (data.isAdmin === false) {
			return false
		} else {
			let updatedTreatment = {
				isActive: reqBody.isActive,
				price: reqBody.price
			}
			return Treatment.findByIdAndUpdate(data.treatmentId, updatedTreatment).then((result,error) => {
				if (error) {
					return false
				} else {
					return result
				}
			})
		}
	})
}


// module.exports.updateTreatment = (reqParams, reqBody) => {
// 	let updatedTreatment = {
// 		isActive: reqBody.isActive
// 		// , treatmentName: reqBody.treatmentName
// 	}
// 	return Treatment.findByIdAndUpdate(reqParams.treatmentId, updatedTreatment).then((result,error) => {
// 		if (error) {
// 			return false 
// 		} else {
// 			return true
// 		}
// 	})
// }

//Update Treatment - PUT - http://localhost:4000/api/treatments/6273373b8d7f3d3c72e74b95
// Diathermy --- fr 150 to 155
// auth (non-Admin)
//output:
// false
// auth (Admin)
//output:
//
// ********************************************
// Get All Treatments

module.exports.getAllTreatments = () => {
	return Treatment.find( {} ).then((result, error) => {
		if (error) {
			return false
		}else{
			return result
		}
	})
}

// ********************************************
// Archive Treatment (Admin ONLY)

module.exports.archiveTreatment = ( data, reqBody ) => {
	
	return User.findById(data.userId).then(result => {
		if (data.isAdmin === false) {
			return false
		} else {
			let updatedTreatmentStatus = {
				isActive: reqBody.isActive
			}
		  return Treatment.findByIdAndUpdate(data.treatmentId, updatedTreatmentStatus).then((result, error) => {
			if (error) {
				return false
			} else {
				return result
			}
		})
	 }

  })
}

//prev code
// module.exports.archiveTreatment = ( reqParams, reqBody ) => {
// 	let updatedTreatmentStatus = {
// 		isActive: reqBody.isActive
// 	}
// 	return Treatment.findByIdAndUpdate(reqParams.treatmentId, updatedTreatmentStatus).then(
// 		(result, error) => {
// 			if (error) {
// 				return false
// 			} else {
// 				return result
// 		}
// 	})
// }

//Archive Treatment - PUT - http://localhost:4000/api/treatments/62733a9c8d7f3d3c72e74b9b/archive
//Group therapy  --- isActive fr true to false
//auth(nonAdmin)
//{
//     "isActive": false
// }
//output:
//false
//auth(Admin)
//output:
// {
//     "_id": "62733a9c8d7f3d3c72e74b9b",
//     "treatmentName": "Group therapy",
//     "description": "The treatment of 2-6 patients, who are performing the same or similar activities and are supervised by a therapist or an assistant.",
//     "price": 40,
//     "isActive": false,
//     "createdOn": "2022-05-05T02:24:51.584Z",
//     "patients": [],
//     "__v": 0
// }
// ********************************************




