/* B170-salanga-ecommerce orderController.js */

const User = require("../models/User.js")
const Treatment = require("../models/Treatment.js")
const Order = require("../models/Order.js")

const Cart = require('../models/Cart');
const cartController = require('../controllers/cartController')



const auth = require("../auth.js")
const bcrypt = require("bcrypt")

// ********************************************
// Get Orders of Specific User  (non-Admin ONLY)

// router.get("/myorders", auth.verify,(req, res) => {
// orderController.getOrder(req.body).then(result => res.send(result))
// })

module.exports.getOrder = (reqBody, userData) => {

	return User.findById(userData.userId).then(result => {
			if (userData.isAdmin !== false) {
				return false
				} else {
		
					return Order.find({userId: reqBody.userId}).then(result => {
						if (result === null) {
							return false
						} else {
							return result
						}
					})

			}

	})

}



// ********************************************
// Get All Orders  (Admin ONLY  ====)

// router.get("/getallorders", auth.verify, (req, res) => {
// 	const userData = auth.decode(req.headers.authorization)
// 	orderController.getAllOrders(userData).then(resultFromController => res.send(resultFromController))
// })

module.exports.getAllOrders = (userData) => {
	return User.findById(userData.userId).then(result => {
		if (userData.isAdmin === false) {
			return `PERMISSION DENIED`
		} else {
			return Order.find( {} ).then((result, error) => {
				if (error) {
					return false
				}else{
					return result
				}
			})

		}
	})

}

// ********************************************
// Create Order (non-Admin ONLY)

// router.post("/checkout", auth.verify, (req, res) => {
// 	let data = {
// 		userId: auth.decode(req.headers.authorization).id,
// 		isAdmin: auth.decode(req.headers.authorization).isAdmin,
// 		treatmentId: req.body.treatmentId,
// 		treatmentName: req.body.treatmentName,
// 		quantity: req.body.quantity,
// 		price: req.body.price
// 	}
// 	orderController.createOrder(data).then(result => res.send (result))
// })

// ************************************************************************

module.exports.createOrder = (data) => {
	return User.findById(data.userId).then(user => {
		if (data.isAdmin !== false) {
			return false
		} else {

// =========================			
			let isUserUpdated = User.findById(data.userId).then(user => {
				user.treatments.push({treatmentId: data.treatmentId})
				//order.orderTreatments.push({treatmentId: data.treatmentId})

				return user.save().then((user, err) => {
					if (err) {
						return false
					} else {
						return true
					}
				
				})
			})
// =========================
			let isTreatmentUpdated = Treatment.findById(data.treatmentId).then(treatment => {
				treatment.patients.push({userId: data.userId})
				//order.orderUsers.push({userId: data.userId})

				return treatment.save().then ((treatment, err) => {
					if (err) {
						return false
					} else {
						return true
					}
				})
			})

////		
// =========================
			const newOrder = new Order ({
				userId: data.userId,
				treatmentId: data.treatmentId,
				treatmentName: data.treatmentName,
				quantity: data.quantity,
				price: data.price,				
				totalBill: data.price * data.quantity
			})

			//newOrder.items.push({userId: data.userId})

			// let isOrderUpdated = Order.findbyId(data.userId).then(user => {
			// 	user.items.push({treatmentId: data.treatmentId})
			// 	user.items.push({treatmentName: data.treatmentName})

				// Order.items.push(newOrder)
			
				return newOrder.save().then((order, err) => {
					if (err) {
						return false
					} else {
						return true
					}
				})
			
// =========================
				// if (isUserUpdated && isTreatmentUpdated) {
				if (isUserUpdated && isTreatmentUpdated && newOrder) {
				
				return true				

			}	else {
				return false
			}		
// =========================


		
		}

	})

}

// **********************************************************************






// ********************************************
/* !!!! ADDED ON 09JUN2022  */

module.exports.checkout = (payload) => {
	return Cart.findOne({userId: payload.id}).then(result => {
		// findOne returns null if cart is not found. Null is falsy. A returned object(even if empty) is a truthy.
		if(result) {
			const {userId, treatments, totalBill} = result;

			let order = new Order({
				userId: userId,
				treatments: treatments,
				totalBill: totalBill
			});

			return order.save().then((order, err) => {
				if(err) {
					return false;
				}
				else {
					console.log(order.userId);
					cartController.deleteCart(order.userId);
					return order;
				}
			})
		}
		else {
			return false;
		}
	})
}


// ********************************************







