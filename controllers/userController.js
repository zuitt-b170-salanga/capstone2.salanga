/* B170-salanga-ecommerce userController.js */

const User = require("../models/User.js")
const Treatment = require("../models/Treatment.js")
const auth = require("../auth.js")
const bcrypt = require("bcrypt")

// ********************************************
// User registration (Admin & non-Admin)

module.exports.registerUser = (reqBody) => {
	let newUser = new User ({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		age: reqBody.age,
		gender: reqBody.gender,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10),
		referringPhysician: reqBody.referringPhysician,
		diagnosis: reqBody.diagnosis
	})
	// return newUser.save().then((saved, error) => {
		return newUser.save().then((result, error) => {
		if (error) {
			console.log(error)
			return false
		} else {
			//return true
			return result
		}
	})
}

// ********************************************
// User authentication (Admin & non-Admin)

module.exports.userAuthenticate = (reqBody) => {
	return User.findOne({email:reqBody.email}).then(result => {
		if (result === null) {
			return false
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
		
			if (isPasswordCorrect) {
				return {access: auth.createAccessToken(result.toObject())}
			} else {
				return false
			}
		}
	})
}

// ********************************************

// check if the email exists

module.exports.checkEmail = (requestBody) => {
	return User.find({ email: requestBody.email }).then((result, error) => {
		if (error) {
			console.log(error)
			return false
		}else{
			if (result.length > 0) {
				// return result
				return true
			} else {
				// return res.send("email does not exist")
				return false
			}
		}
	})
}

// ********************************************
// Set User as admin (Admin ONLY)

/*
router.put("/:userId/setAsAdmin", auth.verify, (req, res) => {
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	let userData = {
		userId: req.params.userId,
		isAdmin: req.body.isAdmin
	}
	userController.changeToAdmin(data, userData).then(result => res.send(result))
})
*/

module.exports.changeToAdmin = (data, userData, reqBody) => {
	
	return User.findById(data.userId).then(result => {

		if (data.isAdmin === false) {
			//return "PERMISSION DENIED"
			return false
			
		} else  
			{
			let updatedUserStatus = {
				isAdmin: reqBody.isAdmin
			}

			return User.findByIdAndUpdate(userData.userId,updatedUserStatus).then((result, error) => {
				if (error) {
					return false
				} else {
					return true
				}

			})

		}
	
	


	})

}


// ********************************************
// Get profile of single user ----->

module.exports.getProfile = (data) => {
	return User.findById(data.userId).then(result => {
		// if there is no user found...
		if(result === null) {
				return false
			// if a user is found...	
			} else {
				result.password = "";
				return result
		}
	})
}

// ********************************************
// Get all users

module.exports.getAllUsers = () => {
	return User.find( {} ).then((result, error) => {
		if (error) {
			return false
		}else{
			return result
		}
	})
}

// ********************************************

// Create Order (non-Admin ONLY)

// router.post("/checkout", auth.verify, (req, res) => {
// 	let data = {
// 		userId: auth.decode(req.headers.authorization).id,
// 		isAdmin: auth.decode(req.headers.authorization).isAdmin,
// 		treatmentId: req.body.treatmentId,
// 		treatmentName: req.body.treatmentName,
// 		quantity: req.body.quantity,
// 		price: req.body.price
// 	}
// 	orderController.createOrder(data).then(result => res.send (result))
// })

// ************************************************************************


