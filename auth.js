/* B170-salanga-ecommerce auth.js */

const jwt = require("jsonwebtoken")
const secret = "IDoNotKnow" // form of message/string in w/c this will serve as our secret code

// token creation ---->

module.exports.createAccessToken = (user) => {
	// data will be recvd from the registration form, when the user login, a token will be created w/ the user's info (this info still encrypted inside the token)
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}
	// generates the token using the form data & the secret code w/o additional options
	return jwt.sign(data,secret,{})
}


// token verification ---->
 
module.exports.verify = (req, res, next) => {  // next parameter - tells the server to proceed if the verification is okay/successful 
	let token = req.headers.authorization     //  authorization can be found in 	the headers of the rqst & tells whether the client/user has the authority to send the rqst

	if(typeof token !== "undefined") {      // token is present
		console.log(token)
		token = token.slice(7, token.length)
		// jwt.verify - verifies the token using the secret & fails if the token's secret does not match the secret variable meaning there is an attempt hack, or at least to tamper/change the data fr the user-end  
		return jwt.verify(token, secret, (err, data) => {
			if (err) {
				return res.send({auth:"failed"})
			} else {
				// tells the server to proceed to processing of the rqst
				next();
			}
		})
	}
}


// token decoding ---->

module.exports.decode = (token) => {
	if (typeof token !== "undefined") {    // token is present
		token = token.slice(7, token.length)
		return jwt.verify(token, secret, (err, data) =>{
			if (err) {
				console.log(err)
				return null
			} else {
				return jwt.decode(token, {complete:true}).payload
				// jwt.decode - decides the data to be decoded, w/c is the token
				// payload - is the one that we need to verify the user info this is a part of the token when code the createAccessToken function (the one w/ _id, email, & isAdmin)
			}
		})
	} else {
		return null    // there is no token
	}
}
