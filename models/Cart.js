/* B170-salanga-ecommerce Cart.js */

/* !!!! ADDED ON 09JUN2022  */

const mongoose = require('mongoose');


const cartSchema = new mongoose.Schema({
    userId: {
        type: String,
        required: true
    },
    treatments: [{
        treatmentId: {
            type: String
        },
        name: String,
        quantity: {
            type: Number, 
            required: true,
            min: [1, 'Quantity cannot be less than 1'],
            default: 1
        },
        price: {
            type: Number,
            required: true
        }
        
    }],
    totalBill: {
        type: Number,
        required: true
    }
})

module.exports = mongoose.model('Cart', cartSchema);
