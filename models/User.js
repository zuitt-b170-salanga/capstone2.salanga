/* B170-salanga-ecommerce User.js */


const mongoose = require("mongoose")

////
const ObjectID = mongoose.Schema.Types.ObjectId
////

const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, "First name is required"]
	},
	
	lastName: {
		type: String,
		required: [true, "Last name is required"]
	},
	
	age: {
		type: String,
		required: [true, "Age is required"]
	},

	gender: {
		type: String,
		required: [true, "Gender is required"]
	},

	email: {
		type: String,
		required: [true, "Email is required"]
	},

	password: {
		type: String,
		required: [true, "Password is required"]
	},

	referringPhysician: {
		type: String,
		required: [true, "Referring Physician is required"]
	},

	diagnosis:{
		type: String,
		required: [true, "Diagnosis is required"]
	},

	isAdmin: {
		type: Boolean,
		default: false
	},

	treatments: [
		
		{
			treatmentId: {
				type: String,
				required: [true, "Treatment ID is required"]
			},
			
			treatedOn: {
				type: Date,
				default: new Date()
			},
			
			status: {
				type: String,
				default: "Active"
			}
		
		}

	]
})

module.exports = mongoose.model("User", userSchema );