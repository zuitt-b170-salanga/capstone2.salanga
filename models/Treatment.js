/* B170-salanga-ecommerce Treatment.js */

const mongoose = require("mongoose")

////
const ObjectID = mongoose.Schema.Types.ObjectId
////

const treatmentSchema = new mongoose.Schema({

	

	treatmentName: {
		type: String,
		required: [true, "Treatment name is required"]
	},
	
	description: {
		type: String,
		required: [true, "Treatment description is required"]
	},
	
	price: {
		type: Number,
		required:[true, "Price for the treatment is required"]
	},

	isActive: {
		type: Boolean,
		default: true
	},

	createdOn: {
		type: Date,
		default: new Date()
	},


	patients: [
		{
			userId: {
				type: String,
				required: [true, "User ID is required"]
			},
		
			treatedOn: {
				type: Date,
				default: new Date()
			}
		}	
	]
})


module.exports = mongoose.model("Treatment", treatmentSchema )
