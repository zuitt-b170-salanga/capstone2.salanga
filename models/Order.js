/* B170-salanga-ecommerce Order.js */



// ******************************************************


const mongoose = require ("mongoose")

const orderSchema = new mongoose.Schema ({

userId: {
	type: String,
	required: [true, "User ID is required"]
	},


treatments:	[{ 

	treatmentId: {
		type: String,
		//required: [true, "Treatment ID is required"]
	},
	
	treatmentName: {
		type: String,
		//required: [true, "Treatment name is required"]
	},

	quantity: {
		type: Number,
		required: true,
		min: 1,
		default: 1
	},
	price: {
		type: Number,
		required: true
	}

}],


totalBill: {
	type: Number,
	required: true
},

orderedOn: {
		type: Date,
		default: new Date()
	},

isActive: {
		type: Boolean,
		default: true
	}

})

module.exports = mongoose.model("Order", orderSchema)

// ******************************************************





